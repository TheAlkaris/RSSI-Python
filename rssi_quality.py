#!/usr/bin/env python3
# coding: utf-8

import os
import platform as plat
from enum import Enum
import re


class bcolor(Enum):
    UNS = '\033[1;37;41m'
    LOW = '\033[1;31m'
    MED = '\033[1;34m'
    HIGH = '\033[1;32m'
    ERR = '\033[1;35m'
    YEL = '\033[1;33m'
    WHT = '\033[1;37m'
    ENDC = '\033[0m'


def get_wireless_interface():
    system = plat.system()
    if system == 'Linux':
        cmd = "iwconfig | grep '802.11' | awk '{print $1}'"
    elif system == 'Darwin':
        cmd = "networksetup -listallhardwareports | grep -A1 Wi-Fi | awk '/Device/ { print $NF }'"
    else:
        return None

    interface = os.popen(cmd).read().strip()
    if interface:
        return interface
    return None


def get_wifi_signal_strength(interface):
    system = plat.system()
    if system == 'Linux':
        cmd = "iwconfig {} | grep Signal | awk '{{print $4}}' | cut -d'=' -f2".format(interface)
    elif system == 'Darwin':
        cmd = "/System/Library/PrivateFrameworks/Apple80211.framework/Versions/Current/Resources/airport -I | grep CtlRSSI | awk '{{ print $NF; }}'"
    else:
        return None

    dbm = os.popen(cmd).read()
    if dbm:
        dbm_num = int(dbm)
        quality = 2 * (dbm_num + 100)
        return dbm_num, quality
    return None


def print_legend():
    print('\n' + bcolor.WHT.value + '   [ RSSI Quality Guide ]  ' + bcolor.ENDC.value + '\n')
    print('  -96dBm ≠ 8 %' + '\t' + bcolor.UNS.value + '[ Unusable ]' + bcolor.ENDC.value)
    print('  -85dBm ≠ 30%' + '\t' + bcolor.LOW.value + '[ Low ]' + bcolor.ENDC.value)
    print('  -75dBm ≠ 50%' + '\t' + bcolor.MED.value + '[ Medium ]' + bcolor.ENDC.value)
    print('  -55dBm ≠ 90%' + '\t' + bcolor.HIGH.value + '[ High ]' + bcolor.ENDC.value)


def print_wifi_signal(dbm_num, quality):
    if quality <= 8:
        color = bcolor.UNS
    elif quality <= 30:
        color = bcolor.LOW
    elif quality <= 50:
        color = bcolor.MED
    else:
        color = bcolor.HIGH

    print('\n' + bcolor.YEL.value + 'Current:' + bcolor.ENDC.value + ' {} dBm | {} %'.format(color.value + str(dbm_num) + bcolor.ENDC.value, color.value + str(quality) + bcolor.ENDC.value) +'\n')


def main():
    interface = get_wireless_interface()
    if interface:
        signal = get_wifi_signal_strength(interface)
        if signal:
            dbm_num, quality = signal
            print_legend()
            print_wifi_signal(dbm_num, quality)
        else:
            print(bcolor.ERR.value + 'Not in range or no Wi-Fi signal.' + bcolor.ENDC.value)
    else:
        print(bcolor.ERR.value + 'Wireless interface not found.' + bcolor.ENDC.value)


if __name__ == '__main__':
    main()
