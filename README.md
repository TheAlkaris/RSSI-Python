[![pipeline status](https://gitlab.com/TheAlkaris/RSSI-Python/badges/master/pipeline.svg)](https://gitlab.com/TheAlkaris/RSSI-Python/commits/master)
# RSSI Quality Check in Python
Some simple RSSI quality checker written in Python 3.x and to be made work with Ncurses eventually.


### Info:
Insert random bullshit info here I guess?

Nothing special to be planned for this, just a little side project as I learn to code better in Python and get to grips with it more. I may make better use out of this script I have conjured up piecing together what information I have learned thus far.

main purpose of this python script is to check currently connected RSSI signal quality of Wi-Fi you're currently connected to like so in the following preview image...

[![RSSi Quality Checker in Python][1]][1]

  [1]: /flameshot_2023-06-21__11_33_22_AM.png
  
Clone the repo to your local directory:

```HTTP
https://gitlab.com/TheAlkaris/RSSI-Python.git
```

run the script:
```PYTHON
python ./rssi_quality.py
```
give the script `u+x` permission and you can just run `./rssi_quality.py`
